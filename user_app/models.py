from django.db import models
from django.contrib.auth.models import AbstractUser
from user_app.manager import UserManager


# Custom User Model

class UserModel(AbstractUser):
    objects = UserManager()

    id = models.AutoField(primary_key=True, editable=False, unique=True)
    first_name = models.CharField(max_length=50, blank=True, default=None)
    last_name = models.CharField(max_length=50, blank=True,default=None)
    email = models.EmailField(max_length=100, unique=True, blank=False)
    password = models.CharField()

    REQUIRED_FIELDS = ['first_name', 'last_name', 'email']

    class Meta:
        db_table = "user_tb1"


